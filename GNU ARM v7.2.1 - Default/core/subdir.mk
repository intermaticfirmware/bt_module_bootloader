################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4/platform/bootloader/core/btl_core.c \
C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4/platform/bootloader/driver/btl_driver_util.c \
../core/btl_main.c 

OBJS += \
./core/btl_core.o \
./core/btl_driver_util.o \
./core/btl_main.o 

C_DEPS += \
./core/btl_core.d \
./core/btl_driver_util.d \
./core/btl_main.d 


# Each subdirectory must supply rules for building sources it contributes
core/btl_core.o: C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4/platform/bootloader/core/btl_core.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g3 -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__STARTUP_CLEAR_BSS=1' '-D__START=main' '-DBOOTLOADER_SECOND_STAGE=1' '-DEFR32BG13P632F512GM48=1' '-DBOOTLOADER_VERSION_MAIN_CUSTOMER=7' '-DBTL_CONFIG_FILE="bootloader-configuration.h"' '-DBTL_SLOT_CONFIGURATION="bootloader-slot-configuration.h"' '-DEM_MSC_RUN_FROM_FLASH=1' '-DMBEDTLS_CONFIG_FILE="config-sl-crypto-all-acceleration.h"' '-DHAL_CONFIG=1' '-DEMBER_AF_USE_HWCONF=1' -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\hal-config" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\core_inc" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/CMSIS/Include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//hardware/module/config" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/halconfig/inc/hal-config" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4///platform/Device/SiliconLabs/EFR32BG13P/Include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/bootloader/." -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/configs" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/sl_crypto/include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/emdrv/common/inc" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/emlib/inc" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\hal-config" -Os -Wall -Wextra -Werror -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"core/btl_core.d" -MT"core/btl_core.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

core/btl_driver_util.o: C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4/platform/bootloader/driver/btl_driver_util.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g3 -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__STARTUP_CLEAR_BSS=1' '-D__START=main' '-DBOOTLOADER_SECOND_STAGE=1' '-DEFR32BG13P632F512GM48=1' '-DBOOTLOADER_VERSION_MAIN_CUSTOMER=7' '-DBTL_CONFIG_FILE="bootloader-configuration.h"' '-DBTL_SLOT_CONFIGURATION="bootloader-slot-configuration.h"' '-DEM_MSC_RUN_FROM_FLASH=1' '-DMBEDTLS_CONFIG_FILE="config-sl-crypto-all-acceleration.h"' '-DHAL_CONFIG=1' '-DEMBER_AF_USE_HWCONF=1' -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\hal-config" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\core_inc" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/CMSIS/Include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//hardware/module/config" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/halconfig/inc/hal-config" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4///platform/Device/SiliconLabs/EFR32BG13P/Include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/bootloader/." -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/configs" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/sl_crypto/include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/emdrv/common/inc" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/emlib/inc" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\hal-config" -Os -Wall -Wextra -Werror -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"core/btl_driver_util.d" -MT"core/btl_driver_util.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

core/btl_main.o: ../core/btl_main.c
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM C Compiler'
	arm-none-eabi-gcc -g3 -gdwarf-2 -mcpu=cortex-m4 -mthumb -std=c99 '-D__STARTUP_CLEAR_BSS=1' '-D__START=main' '-DBOOTLOADER_SECOND_STAGE=1' '-DEFR32BG13P632F512GM48=1' '-DBOOTLOADER_VERSION_MAIN_CUSTOMER=7' '-DBTL_CONFIG_FILE="bootloader-configuration.h"' '-DBTL_SLOT_CONFIGURATION="bootloader-slot-configuration.h"' '-DEM_MSC_RUN_FROM_FLASH=1' '-DMBEDTLS_CONFIG_FILE="config-sl-crypto-all-acceleration.h"' '-DHAL_CONFIG=1' '-DEMBER_AF_USE_HWCONF=1' -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\hal-config" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\core_inc" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/CMSIS/Include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//hardware/module/config" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/halconfig/inc/hal-config" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4///platform/Device/SiliconLabs/EFR32BG13P/Include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/bootloader/." -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/configs" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//util/third_party/mbedtls/sl_crypto/include" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/emdrv/common/inc" -I"C:/SiliconLabs/SimplicityStudio/v4/developer/sdks/blemesh/v1.4//platform/emlib/inc" -I"C:\Users\reverett\SimplicityStudio\firmware_up_hmi\bt_module_bootloader\hal-config" -Os -Wall -Wextra -Werror -c -fmessage-length=0 -ffunction-sections -fdata-sections -mfpu=fpv4-sp-d16 -mfloat-abi=softfp -MMD -MP -MF"core/btl_main.d" -MT"core/btl_main.o" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


