# bt_module_bootloader #

this repo contains the bootloader used for hmi, sensor, and lighting controller

### flash initial bootloader and application ###

to flash initial bootloader and application:

1. Build bootloader
2. Build application
3. Merge the (combined) bootloader and the application image: commander convert bootloader-uart-bgapi-combined.s37 your_application.s37 -o app+bootloader.s37
4.	Double click commander.exe 
5.	Adapter connect
6.	Target connect
7.	Select Flash and app+bootloader.s37
8.	Flash start address 0, Reset MCU after flashing should be checked, Verify upload should be checked
9.	Press Flash button

### create update files ###

to create update files:

1. copy bt_module_bootloader.s37 to root application folder
2. execute batch file create_bl_files.bat
3. copy files in folder output_gbl to blue gecko application folder 
4. update using blue gecko app

### updating bootloader ###

to update bootloader open bootloader-storage-internal-ble.isc go to other tab
and increment the BOOTLOADER_VERSION_MAIN_CUSTOMER value and rebuild bootloader application

### debug output ###

to see the debug output of the bootloader open commander and connect using swd
change freq to 800 kHz should see "Enter app" displayed 
